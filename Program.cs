﻿using System;
using System.Diagnostics;
using System.Numerics;
using System.Threading;

namespace Performance_Comparator
{
	public class Program
	{
		static void printTab()
		{
			Console.Write("   ");
		}
		static void Main(string[] args)
		{

			///OPERATIVE VARIABLES
			int test1WinCount = 0;
			double test1RoundTime = 0;
			double test1Total = 0, test2Total = 0;
			double totalDifferent = 0;

			for(int x = 0; x < iterations; x++)
			{
				Console.WriteLine("Iteration #" + (x + 1));
				//set up the test data to operate on, and the storage for test results.
				Testdata[] tests = new Testdata[totalTests];
				Testresult[] results1;
				Testresult[] results2;

				if (resultsCheck)
				{
					results1 = new Testresult[totalTests];
					results2 = new Testresult[totalTests];
				}
				for (int i = 0; i < totalTests; i++)
				{
					tests[i] = Testdata.fuzzRandom();
				}

				Stopwatch stopWatch = new Stopwatch();

				stopWatch.Start();
				for (int i = 0; i < totalTests; i++)
				{
					Testresult result = test1(tests[i]);
					if (resultsCheck)
						results1[i] = result;
				}

				stopWatch.Stop();

				TimeSpan ts = stopWatch.Elapsed;
				printTab();
				Console.WriteLine("test1 RunTime " + ts.TotalMilliseconds);
				test1Total += ts.TotalMilliseconds;
				test1RoundTime = ts.TotalMilliseconds;

				stopWatch = new Stopwatch();

				stopWatch.Start();
				for (int i = 0; i < totalTests; i++)
				{
					Testresult result = test2(tests[i]);
					if (resultsCheck)
						results2[i] = result;
				}

				stopWatch.Stop();

				ts = stopWatch.Elapsed;
				// Format and display the TimeSpan value.
				printTab();
				Console.WriteLine("test2 RunTime " + ts.TotalMilliseconds);
				test2Total += ts.TotalMilliseconds;
				if (ts.TotalMilliseconds > test1RoundTime)
				{
					test1WinCount++;
					Console.WriteLine("Test 1 Wins!");
				} else
				{
					Console.WriteLine("Test 2 Wins!");
				}

				int numberDifferent = 0;
				if (resultsCheck)
				{

					for (int i = 0; i < totalTests; i++)
					{
						if (results1[i].doesNotMatch(results2[i]))
						{
							numberDifferent++;
							printTab();
							Console.WriteLine("sadly, tests are not exactly the same - different outputs.");
							tests[i].print();
							results1[i].print();
							results2[i].print();
						}
					}
					totalDifferent += numberDifferent;
					Console.WriteLine(numberDifferent + "/" + totalTests + " tests were different results");
					Console.WriteLine();
				}
			}
			if (resultsCheck)
			{
				double percentDifferent = totalDifferent / ((iterations + 1) * totalTests);
				percentDifferent = percentDifferent * 100;
				Console.WriteLine("Percent Different: " + percentDifferent + "% (" + totalDifferent + "/" + (((double)iterations) * (double)totalTests) + ")");
			}

			double test1Average = test1Total / iterations, test2Average = test2Total / iterations;
			Console.WriteLine("Test 1 average: " + test1Average);
			Console.WriteLine("Test 2 average: " + test2Average);

			if (test1Average > test2Average)
			{
				Console.WriteLine("Test 2 is faster!");
				double percent = ((double)iterations - (double)test1WinCount) / (double)iterations * 100;
				Console.WriteLine("Test 2 total wins: " + percent + "% " + (iterations - test1WinCount) + "/" + iterations);
			}
			if (test2Average > test1Average)
			{
				Console.WriteLine("Test 1 is faster!");
				double percent = (double)test1WinCount / (double)iterations * 100;
				Console.WriteLine("Test 1 total wins: " + percent + "% " + test1WinCount + "/" + iterations);
			}

			Console.WriteLine("Press enter to exit");
			Console.ReadLine();
			Environment.Exit(0);
		}
		
			// tweaky spinny handles to test your code with - these three should be the only code that you need to change as far as testing parameters
			const int totalTests = 50;
			const int iterations = 50000;
			const bool resultsCheck = true;


		// BELOW is the classes you need to implement to fill in the code you want to test, to see which implementation is faster, along with an example testing code.

		public class Testdata
		{
			public Vector3 sub = new Vector3();
			public Vector3 node = new Vector3();
			public static double random(Random rand, double min, double max)
			{
				return rand.NextDouble() * (max - min) + min;

			}
			public static Testdata fuzzRandom()
			{
				Testdata result = new Testdata();
				Random autorand = new Random();
				
				result.sub.X = (float)random(autorand, -10, 10);
				result.sub.Y = (float)random(autorand, -10, 10);
				result.sub.Z = (float)random(autorand, -10, 10);

				result.node.X = (float)random(autorand, -1, 1);
				result.node.Y = (float)random(autorand, -1, 1);
				result.node.Z = (float)random(autorand, -1, 1);
				return result;
			}
			public void print()
			{
				printTab();
				printTab();
				Console.WriteLine("sub = " + sub.ToString());
				printTab();
				printTab();
				Console.WriteLine("node = " + node.ToString());
			}
		}
		public class Testresult
		{
			public sbyte result = 0;
			public bool doesNotMatch(Testresult other)
			{
				if (result == other.result)
				{
					return false;
				}
				return true;
			}
			public void print()
			{
				printTab();
				printTab();
				Console.WriteLine("Result = " + result);
			}

		}

		public static Testresult test1(Testdata data)
		{
			Testresult result = new Testresult();
			Vector3 node = data.node;
			Vector3 sub = data.sub;
			if (sub.Z == node.Z | sub.Y == node.Y | sub.X == node.X)
			{
				result.result = -1;
				return result;
			}
			result.result = 0;
			if (sub.X < node.X)
				result.result++;
			if (sub.Y < node.Y)
				result.result += 2;
			if (sub.Z > node.Z)
				result.result += 4;
			return result;
		}

		public static Testresult test2(Testdata data)
		{
			Testresult result = new Testresult();
			result.result = -1;
			Vector3 node = data.node;
			Vector3 sub = data.sub;
			if (sub.X > node.X)
			{
				if (sub.Y > node.Y)
				{
					if (sub.Z > node.Z)
					{
						result.result = 4;
						return result;
					}
					else if (sub.Z < node.Z)
					{
						result.result = 0;
						return result;
					}

				}
				else if (sub.Y < node.Y)
				{

					if (sub.Z > node.Z)
					{
						result.result = 6;
						return result;
					}
					else if (sub.Z < node.Z)
					{
						result.result = 2;
						return result;
					}
				}
			}
			else if (sub.X < node.X)
			{
				if (sub.Y > node.Y)
				{
					if (sub.Z > node.Z)
					{
						result.result = 5;
						return result;
					}
					else if (sub.Z < node.Z)
					{
						result.result = 1;
						return result;
					}

				}
				else if (sub.Y < node.Y)
				{

					if (sub.Z > node.Z)
					{
						result.result = 7;
						return result;
					}
					else if (sub.Z < node.Z)
					{
						result.result = 3;
						return result;
					}
				}
			}

			return result;
		}

	}
}
